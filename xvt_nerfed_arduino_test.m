function xvt_nerfed_arduino_test


debug_mode = false; %will run PTB in windowed mode so we can use GUI after dbstop if error
rng('shuffle');

grayval = 127; 
resolution = [1920 1080]; %x-resolution, y-resolution; must provide both

% timing variables -- all in seconds
stim_time_onscreen = 1; 
flip_timewindow = .006;
initial_wait = 5; 

% fixation rect
inner_fixation_color = [255 255 255]; %white
inner_fixation_radius = 3; %pixels
outer_fixation_radius = 4; %pixels
outer_fixation_color = [0 0 0];

% arduino stuff
arduino_device_name = '/dev/serial/by-id/usb-Arduino__www.arduino.cc__0043_95437313135351411222-if00';
disable_eeg_codes = (1 == 0); %doing it this way because literal true/false causes a code analyzer error
run_max_n_codes = 10000; %should never be anywhere near this many
clear global g_current_code_index g_all_codes_dec g_all_code_timestamps;
global g_n_codes_sent g_all_codes_dec g_all_code_timestamps;
g_n_codes_sent = 0;
g_all_codes_dec = nan(run_max_n_codes,1);
g_all_code_timestamps = nan(run_max_n_codes,1);

%key parameters
triggerkey= '*'; %same key as scanner trigger


n_trials = 5; % just for test
stim_onset_times = nan(n_trials,1);


%====================================
% initialize Psychtoolbox
%====================================
magic_cleanup = onCleanup(@mj_ptb_cleanup);
[win, wRect]=mj_ptb_init(grayval,resolution,debug_mode); %#ok<ASGLU>
if disable_eeg_codes
    my_serial_port = '';
else
    my_serial_port = IOPort('OpenSerialPort', arduino_device_name, 'BaudRate=115200'); %possibly stick Lenient back in but let's see if we can live without it
end

%=====================================
% check refresh rate before continuing
%=====================================
flip_interval=Screen('GetFlipInterval',win);
refresh_rate=1/flip_interval;
keypress=mj_prompt_key_code(win,[ 'Current refresh rate: about ' int2str(round(refresh_rate)) ' Hz\n'...
                                        'Press q to quit and change refresh rate,\n'...
                                        'any other key to begin']);
if strcmp(KbName(keypress),'q') || strcmp(KbName(keypress),'Q')
    error('User quit task');
end

% rect creation
center_point = [resolution(1)/2, resolution(2)/2];
inner_fixation_rect = mj_circle_rects_from_points(center_point(1), center_point(2), inner_fixation_radius);
outer_fixation_rect = mj_circle_rects_from_points(center_point(1), center_point(2), outer_fixation_radius);

%===================================
% wait for trigger key to be pressed
%===================================
triggercode=KbName(triggerkey);
keypress=[];

while isempty(keypress) || ~isscalar(keypress) || keypress~=triggercode
    [keypress, start_time]=mj_prompt_key_code(win, 'Waiting to begin');
end

%========================================
% 'task will begin' screen, set wait_till
%========================================
mj_draw_text(win,'Task will begin shortly');
%toggle serial port several times to indicate start of session
mj_send_code( my_serial_port, 0 );
WaitSecs(.5);
mj_send_code( my_serial_port, 255 );
WaitSecs(.5);
mj_send_code( my_serial_port, 0 );
WaitSecs(.5);
mj_send_code( my_serial_port, 255 );
WaitSecs(.5);
mj_send_code( my_serial_port, 0 );
WaitSecs(.5);
mj_send_code( my_serial_port, 255 );
WaitSecs(.5);
mj_send_code( my_serial_port, 0 );

Screen('Flip',win);
wait_till = start_time + initial_wait - flip_timewindow;

%=========================
% actual trials start here
%=========================

ShowCursor( 'CrossHair' );

for i = 1:n_trials
    Screen('FillOval', win, outer_fixation_color, outer_fixation_rect );
    Screen('FillOval', win, inner_fixation_color, inner_fixation_rect );
    stim_onset_times(i) = Screen('Flip', win, wait_till);
    SetMouse( center_point(1), center_point(2), win );
    
    mj_construct_send_stim_code( my_serial_port, this_run_pairs{i} );

    wait_till = stim_onset_times(i) + stim_time_onscreen - flip_timewindow;
    while GetSecs < wait_till
        [~, ~, mouse_buttons] = GetMouse(win);
        if any(mouse_buttons)
            click_time = GetSecs;
            mouse_buttons_pressed(i) = true; % records if *any* mouse button was pressed
            mouse_click_times(i) = 1000*(click_time - stim_onset_times(i)); % click times in ms
            mouse_click_alert = 1;
            break;
        end
        WaitSecs(.001);
    end
end

Screen('Flip', win, wait_till);
ShowCursor( 'Arrow' );

mj_draw_text(win,'Shutting down task; remain still');

mj_send_code( my_serial_port, 0 );
WaitSecs(.5);
mj_send_code( my_serial_port, 255 );
WaitSecs(.5);
mj_send_code( my_serial_port, 0 );
WaitSecs(.5);
mj_send_code( my_serial_port, 255 );
WaitSecs(.5);
mj_send_code( my_serial_port, 0 );
WaitSecs(.5);
mj_send_code( my_serial_port, 255 );
WaitSecs(.5);
mj_send_code( my_serial_port, 0 );

all_codes_dec = g_all_codes_dec(1:g_n_codes_sent); %#ok<NASGU>
all_code_timestamps = g_all_code_timestamps(1:g_n_codes_sent); %#ok<NASGU>
clear global g_current_code_index g_all_codes_dec g_all_code_timestamps;

stim_onset_times = stim_onset_times - start_time; %#ok<NASGU>


%---------------------------------------------
function [w, wRect]=mj_ptb_init(grayval,new_resolution,debug_mode)

%do various initializations, pop up a window with the specified background
%  grayscale color (0-255). Returns window pointer

% check for OpenGL compatibility, abort otherwise:
AssertOpenGL;

% Make sure keyboard mapping is the same on all supported operating systems
% Apple MacOS/X, MS-Windows and GNU/Linux:
KbName('UnifyKeyNames');

% Get screenNumber of stimulation display. We choose the display with
% the maximum index, which is usually the right one, e.g., the external
% display on a Laptop:
screens=Screen('Screens');
screenNumber=max(screens);

%try to set resolution
if nargin>1 && ~isempty(new_resolution)
    cur_res=Screen('Resolution',screenNumber);
    if cur_res.width ~= new_resolution(1) || cur_res.height ~= new_resolution(2)
        try
            Screen('Resolution',screenNumber,new_resolution(1),new_resolution(2));
        catch %#ok<CTCH>
            button=questdlg(['Unable to set resolution to ' int2str(new_resolution(1)) ' x ' int2str(new_resolution(2)) ...
                '. If eyetracking, fixation calculations will likely be inaccurate. Quit or continue anyway?'],'','Quit','Continue','Quit');
            if isempty(button) || strcmp(button,'Quit')
                error('Unable to set resolution; user quit');
            end
        end
    end
end

% Hide the mouse cursor:
if ~debug_mode
    HideCursor;
end

% Returns as default the mean gray value of screen:
grayval=GrayIndex(screenNumber,grayval/255); 

% Open a double buffered fullscreen window on the stimulation screen
% 'screenNumber' and choose/draw a gray background. 'w' is the handle
% used to direct all drawing commands to that window - the "Name" of
% the window. 'wRect' is a rectangle defining the size of the window.
% See "help PsychRects" for help on such rectangles and useful helper
% functions:
if debug_mode
    [w, wRect]=Screen('OpenWindow',screenNumber,grayval,[50 50 new_resolution(1)+50 new_resolution(2)+50],[],[],[],[],[],kPsychGUIWindow);
else
    [w, wRect]=Screen('OpenWindow',screenNumber,grayval);
end

% Set text size (Most Screen functions must be called after
% opening an onscreen window, as they only take window handles 'w' as
% input:
Screen('TextSize', w, 32);

%don't echo keypresses to Matlab window
if ~debug_mode
    ListenChar(2);
end

% Do dummy calls to GetSecs, WaitSecs, KbCheck to make sure
% they are loaded and ready when we need them - without delays
% in the wrong moment:
KbCheck;
WaitSecs(0.1);
GetSecs;

% Set priority for script execution to realtime priority:
priorityLevel=MaxPriority(w);
Priority(priorityLevel);

if IsWin
    ShowHideWinTaskbarMex(0);
end

%-------------------------------------------------
function mj_ptb_cleanup

disp('Doing cleanup...');
Screen('CloseAll');
IOPort('CloseAll');
ShowCursor;
fclose('all');
Priority(0);
ListenChar(0);
if IsWin
    ShowHideWinTaskbarMex(1);
end

%-------------------------------------------------
function [code, secs]=mj_prompt_key_code(w,message)

mj_draw_text(w,message);
[secs, code]=KbWait([],2);
code=find(code);

%-------------------------------------------------
function mj_draw_text(w, message)

DrawFormattedText(w, message, 'center', 'center', WhiteIndex(w));
Screen('Flip', w);

%---------------------------------------------------------
function rects=mj_circle_rects_from_points(xs, ys, radius)

rects=  [   xs(:)'-radius;
            ys(:)'-radius;
            xs(:)'+radius;
            ys(:)'+radius   ];

%-------------------------------------------------
function mj_construct_send_stim_code( port, stim_name )

% faces -- 0's
% scenes -- 100's
% things -- 200's
% main runs only:
% 1st item -- 1, 2, 3 (last digit)
% 2nd item -- 4, 5, 6 (last digit)
% strong items -- 40's's
% weak items -- 30's
% practice runs only:
% last 2 digits are stim order in folder, i.e. 1-12 for each ca
% (so practice will be 001 - 012, 101 - 112, 201 - 212)

% stim_name will be in the form of wFace1, sThng6, etc
% s/w/p @ beginning -- strong/weak/practice
% Face/Scne/Thng -- category

stim_type = stim_name(1);
stim_cat = stim_name(2);

if strcmp(stim_type, 'p')
    stim_num = stim_name(end-1:end);
else
    stim_num = stim_name(end);
end

if strcmp(stim_cat, 'F')
    this_stim_code = '0';
elseif strcmp(stim_cat, 'S')
    this_stim_code = '1';
elseif strcmp(stim_cat, 'T')
    this_stim_code = '2';
end

if strcmp(stim_type, 'w')
    this_stim_code = [this_stim_code, '3'];
end
if strcmp(stim_type, 's')
    this_stim_code = [this_stim_code, '4'];
end
this_stim_code = [this_stim_code, stim_num];
%     this_stim_code = [this_stim_code, num2str(mod(trial_num,2))];

this_stim_code = str2double(this_stim_code);

%IOPort('Write', port, uint8(bin2dec(byte)), 2);
mj_send_code( port, this_stim_code );

%--------------------------------------------------
function mj_send_code( port, stim_code )

global g_n_codes_sent g_all_codes_dec g_all_code_timestamps;

if isempty(port)
    timestamp = GetSecs;
    fprintf('Code sending disabled; pretending to write code %.3d to serial port\n', stim_code );
else
    IOPort('Write', port, uint8(stim_code), 2);
    timestamp = GetSecs;
    fprintf('Wrote code %.3d to serial port\n', stim_code );
end

g_n_codes_sent = g_n_codes_sent + 1;
g_all_codes_dec(g_n_codes_sent) = stim_code;
g_all_code_timestamps(g_n_codes_sent) = timestamp;


