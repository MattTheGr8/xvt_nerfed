function xvt_ino_matlab_receive

arduino_device_name = '/dev/cu.usbmodem1461';
grayval = 127; 
resolution = [1920 1080]; %x-resolution, y-resolution; must provide both
debug_mode = false; %will run PTB in windowed mode so we can use GUI after dbstop if error
addpath(fullfile(pwd,'quaternions'));

global win;

magic_cleanup = onCleanup(@mj_ptb_cleanup);
[win, wRect]=mj_ptb_init(grayval,resolution,debug_mode); %#ok<ASGLU>
ShowCursor;
ListenChar(0);

my_serial_port = IOPort('OpenSerialPort', arduino_device_name, 'BaudRate=38400 DTR=0 RTS=0'); %possibly stick Lenient back in but let's see if we can live without it
WaitSecs(.1);
IOPort('ConfigureSerialPort',my_serial_port, 'DTR=1 RTS=1'); %this plus other DTR/RTS above should force Arduino reset on each run?
IOPort('Purge', my_serial_port);

mode=1;

data_buffer=[];

while true
    [data_buffer, mode] = mj_poll_arduino(my_serial_port, data_buffer, mode);
end




function [databuff, mode]=mj_poll_arduino(ser_port,databuff,mode)

if mode==1
    [databuff, mode]=mj_poll_arduino_waitforcode(ser_port,databuff,mode);
elseif mode==2
    [databuff, mode]=mj_poll_arduino_streamtext(ser_port,databuff,mode);
elseif mode==3
    [databuff, mode]=mj_poll_arduino_getquats(ser_port,databuff,mode);
elseif mode==4
    [databuff, mode]=mj_poll_arduino_streambytes(ser_port,databuff,mode);
elseif mode==5
    [databuff, mode]=mj_poll_arduino_get_text_quats(ser_port,databuff,mode);
end




function [databuff, mode]=mj_poll_arduino_waitforcode(ser_port,databuff,mode)

try
    thisdata=IOPort('read',ser_port);
catch %#ok<CTCH>
    return;
end

if ~isempty(thisdata)
    databuff=[databuff thisdata];
    while length(databuff)>=5
        if databuff(1)~=255
            databuff=databuff(2:end);
        elseif databuff(2)~=255
            databuff=databuff(3:end);
        elseif databuff(3)~=255
            databuff=databuff(4:end);
        elseif databuff(4)~=255
            databuff=databuff(5:end);
        else %actual start code received
            mode = databuff(5);
            databuff=databuff(6:end);
            return;
        end
    end
else
    WaitSecs(.005);
end


function [databuff, mode]=mj_poll_arduino_get_text_quats(ser_port,databuff,mode)

persistent quat_buff;
persistent backspace_chars;
if isempty(backspace_chars)
    backspace_chars=0;
end

try
    thisdata=IOPort('read',ser_port);
catch %#ok<CTCH>
    return;
end

if ~isempty(thisdata)
    databuff=[databuff thisdata];
    while numel(databuff) > 0
        this_char = databuff(1);
        databuff = databuff(2:end);
        
        if this_char==254 %end of string
            quat_str = sprintf('%s',quat_buff);
            do_quat_stuff(quat_str);
            
%             if backspace_chars>0
%                 fprintf('%s', repmat(sprintf('\b'),1,backspace_chars));
%             end
%             fprintf('%s',quat_str);
            backspace_chars = numel(quat_buff);
            quat_buff = [];
            mode = 1;
            return;
        else %character data
            quat_buff = [quat_buff this_char]; %#ok<AGROW>
        end
    end
else
    WaitSecs(.005);
end



function do_quat_stuff(quat_text)

global win;
persistent start_time;
persistent baseline_buffer;
persistent baseline_ctr;
persistent baseline_quat;

if isempty(start_time)
    start_time = GetSecs;
    return;
end

if GetSecs < (start_time+5)
    return;
end

quats = str2num(quat_text); %#ok<ST2NM>
if numel(quats)~=4
    return;
end

if isempty(baseline_buffer)
    disp('starting baseline collection');
    baseline_buffer = nan(4,500);
    baseline_buffer(:,1) = quats;
    baseline_ctr = 2;
    Screen('FillOval',win, [64 64 64], [950 530 970 550]);
    Screen('Flip',win);
    return;
end

if GetSecs < (start_time+10)
    baseline_buffer(:,baseline_ctr) = quats;
    baseline_ctr = baseline_ctr + 1;
    return;
end

if baseline_ctr > 0
    disp('end baseline collection');
    Q = baseline_buffer(:,1:(baseline_ctr-1));
    baseline_ctr = 0;
    Q = Q / size(Q,2); %weight equally
    QQ = Q*(Q');
    [baseline_quat,~] = eigs(QQ,1);
    %baseline_quat should be the "average" quat
end

%not at all sure if this is right, might need to switch quats/baseline_quat or something else entirely
quat_diff = quats;
quat_diff(2:4) = -quat_diff(2:4);
% quat_diff = quats .* quat_diff;
quat_diff = qMul(baseline_quat', quat_diff);
% xyz = quatrotate(quat_diff', [0 1 0]); %here, x is screen x but z is screen y
xyz = qRotatePoint([0 1 0], quat_diff); %should be same as previous line but using Matlab Central File Exchange toolbox instead of Matlab Aero toolbox

% if rand<.05
%     disp(xyz);
% end

mult_factor = 1000 / xyz(2);

xdisp = xyz(1)*mult_factor;
ydisp = xyz(3)*mult_factor;

Screen('FillOval',win, [192 192 192], [950-xdisp 530+ydisp 970-xdisp 550+ydisp]);
Screen('Flip',win);







function [databuff, mode]=mj_poll_arduino_streamtext(ser_port,databuff,mode)

try
    thisdata=IOPort('read',ser_port);
catch %#ok<CTCH>
    return;
end

if ~isempty(thisdata)
    databuff=[databuff thisdata];
    while numel(databuff) > 0
        this_char = databuff(1);
        databuff = databuff(2:end);
        
        if this_char==254 %end of string
            fprintf('\n\n');
            mode = 1;
            return;
        else %character data
            fprintf('%c',this_char);
        end
    end
else
    WaitSecs(.005);
end



function [databuff, mode]=mj_poll_arduino_getquats(ser_port,databuff,mode)

persistent backspace_chars;
if isempty(backspace_chars)
    backspace_chars=0;
end

try
    thisdata=IOPort('read',ser_port);
catch %#ok<CTCH>
    return;
end

if ~isempty(thisdata)
    databuff=[databuff thisdata];
    if numel(databuff) > 15
        quat1 = uint8(databuff(1:4));
        quat2 = uint8(databuff(5:8));
        quat3 = uint8(databuff(9:12));
        quat4 = uint8(databuff(13:16));
        databuff = databuff(17:end);
        mode = 1;
        
        quat1 = typecast(quat1, 'single');
        quat2 = typecast(quat2, 'single');
        quat3 = typecast(quat3, 'single');
        quat4 = typecast(quat4, 'single');
        
        str_tmp = sprintf('Matlab quats = %f, %f, %f, %f',quat1,quat2,quat3,quat4);
        if backspace_chars>0
            fprintf('%s', repmat(sprintf('\b'),1,backspace_chars));
        end
        fprintf('%s',str_tmp);
        backspace_chars = numel(str_tmp);
    end
else
    WaitSecs(.005);
end




function [databuff, mode]=mj_poll_arduino_streambytes(ser_port,databuff,mode)

try
    thisdata=IOPort('read',ser_port);
catch %#ok<CTCH>
    return;
end

if ~isempty(thisdata)
    databuff=[databuff thisdata];
    while numel(databuff) > 0
        this_char = databuff(1);
        databuff = databuff(2:end);
        
        if this_char==254 %end of bytes
            mode = 1; %debug, set back to 1 later
            return;
        else %character data
            fprintf('%d\n',this_char);
        end
    end
else
    WaitSecs(.005);
end





%---------------------------------------------
function [w, wRect]=mj_ptb_init(grayval,new_resolution,debug_mode)

%do various initializations, pop up a window with the specified background
%  grayscale color (0-255). Returns window pointer

% check for OpenGL compatibility, abort otherwise:
AssertOpenGL;

% Make sure keyboard mapping is the same on all supported operating systems
% Apple MacOS/X, MS-Windows and GNU/Linux:
KbName('UnifyKeyNames');

% Get screenNumber of stimulation display. We choose the display with
% the maximum index, which is usually the right one, e.g., the external
% display on a Laptop:
screens=Screen('Screens');
screenNumber=max(screens);

%try to set resolution
if nargin>1 && ~isempty(new_resolution)
    cur_res=Screen('Resolution',screenNumber);
    if cur_res.width ~= new_resolution(1) || cur_res.height ~= new_resolution(2)
        try
            Screen('Resolution',screenNumber,new_resolution(1),new_resolution(2));
        catch %#ok<CTCH>
            button=questdlg(['Unable to set resolution to ' int2str(new_resolution(1)) ' x ' int2str(new_resolution(2)) ...
                '. If eyetracking, fixation calculations will likely be inaccurate. Quit or continue anyway?'],'','Quit','Continue','Quit');
            if isempty(button) || strcmp(button,'Quit')
                error('Unable to set resolution; user quit');
            end
        end
    end
end

% Hide the mouse cursor:
if ~debug_mode
    HideCursor;
end

% Returns as default the mean gray value of screen:
grayval=GrayIndex(screenNumber,grayval/255); 

% Open a double buffered fullscreen window on the stimulation screen
% 'screenNumber' and choose/draw a gray background. 'w' is the handle
% used to direct all drawing commands to that window - the "Name" of
% the window. 'wRect' is a rectangle defining the size of the window.
% See "help PsychRects" for help on such rectangles and useful helper
% functions:
if debug_mode
    [w, wRect]=Screen('OpenWindow',screenNumber,grayval,[50 50 new_resolution(1)+50 new_resolution(2)+50],[],[],[],[],[],kPsychGUIWindow);
else
    [w, wRect]=Screen('OpenWindow',screenNumber,grayval);
end

% Set text size (Most Screen functions must be called after
% opening an onscreen window, as they only take window handles 'w' as
% input:
Screen('TextSize', w, 32);

%don't echo keypresses to Matlab window
if ~debug_mode
    ListenChar(2);
end

% Do dummy calls to GetSecs, WaitSecs, KbCheck to make sure
% they are loaded and ready when we need them - without delays
% in the wrong moment:
KbCheck;
WaitSecs(0.1);
GetSecs;

% Set priority for script execution to realtime priority:
priorityLevel=MaxPriority(w);
Priority(priorityLevel);

if IsWin
    ShowHideWinTaskbarMex(0);
end


%-------------------------------------------------
function mj_ptb_cleanup

disp('Doing cleanup...');
Screen('CloseAll');
IOPort('CloseAll');
ShowCursor;
fclose('all');
Priority(0);
ListenChar(0);
if IsWin
    ShowHideWinTaskbarMex(1);
end

